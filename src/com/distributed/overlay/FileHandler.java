package com.distributed.overlay;

import java.io.*;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

/**
 * User: Randima
 * Date: 3/8/15
 * Time: 8:41 AM
 */
public class FileHandler {
    private List<String> fileList;
    private static FileHandler instance;

    private FileHandler() {
        this.fileList = new ArrayList<String>();

    }

    /**
     * Return FileHandler instance
     * @return
     */
    public static FileHandler getInstance(){
        if(instance==null){
            instance=new FileHandler();
            instance.initialize();
        }
        return instance;
    }

    /**
     * initialize class by reading filenames.txt for file list
     */
    private void initialize() {

        FileInputStream inputStream = null;
        try {
            InputStream stream = this.getClass().getResource("/" + "filenames.txt").openStream();
            Scanner sc = new Scanner(stream, "UTF-8");
            String  s;
            while(sc.hasNextLine()) {
                s=sc.nextLine();
                fileList.add(s);

            }
            sc.close();
        } catch (FileNotFoundException e) {
            System.out.println(e.getMessage());
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }

    /**
     * refresh file name list by reading filenames.txt
     * @return
     */
    public boolean refreshList(){
        FileInputStream inputStream = null;
        fileList.clear();
        try {
            inputStream = new FileInputStream("filenames.txt");
            Scanner sc = new Scanner(inputStream, "UTF-8");
            String  s;
            while(sc.hasNextLine()) {
                s=sc.nextLine();
                fileList.add(s);

            }
            inputStream.close();
            sc.close();
            return true;
        } catch (FileNotFoundException e) {
            System.out.println(e.getMessage());
            return false;
        } catch (IOException ioe) {
            System.out.println(ioe.getMessage());
            return false;
        }
    }

    /**
     * return all file names
     * @return
     */
    public List<String> getFileList() {
        return fileList;
    }

    /**
     * return random number of file names
     * @param number
     * @return
     */
    public List<String> getRandomFileList(int number){
        List<String> tmplist=new ArrayList<String>();
        int index,count;
        count=number;
        Random generator=new Random();
        while(count>0){
            index=generator.nextInt(fileList.size());
            if(!tmplist.contains(fileList.get(index))){
                tmplist.add(fileList.get(index));
                count--;
            }
        }
        return tmplist;
    }
}
