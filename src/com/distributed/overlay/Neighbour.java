package com.distributed.overlay;

/**
 * User: Shamika
 * Date: 3/6/15
 * Time: 11:19 AM
 */
public class Neighbour {
    private String ip;
    private int port;
    private String fileName;

    public Neighbour(String ip, int port, String fileName) {
        this.ip = ip;
        this.port = port;
        this.fileName = fileName;
    }

    public String getIp() {
        return ip;
    }

    public int getPort() {
        return port;
    }

    public String getFileName() {
        return fileName;
    }


}
