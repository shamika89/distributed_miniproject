package com.distributed.overlay;

import com.distributed.communication.BSConnector;
import com.distributed.communication.Decoder;
import com.distributed.communication.UDPSender;
import com.distributed.application.FileManager;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.*;
import java.sql.Timestamp;

/**
 * User: Shamika
 * Date: 3/6/15
 * Time: 11:19 AM
 */
public class Node {
    private String ip;
    private int port;
    private String username;
    private ArrayList<Neighbour> neighbours;
    private List<String> files;
    private Map<String,Neighbour> fileTable;
    private BSConnector bsConnector;
    private UDPSender udpSender;
    private Decoder decoder;
    private FileHandler fileHandler;
    private String lastSearch;
    private int messageCount = 0;
    private int forwardMsg = 0;
    private int ansMsg = 0;
    private int hop = 0;
    private int searchOk = 0;
    private long totalTime = 0;
    private Timestamp sendTime, receiveTime;

    /**
     * Constructor
     * @param ip
     * @param port
     * @param username
     */
    public Node(String ip, int port, String username) {
        this.ip = ip;
        this.port = port;
        this.username = username;
        neighbours = new ArrayList<Neighbour>();
        fileHandler = FileHandler.getInstance();
        files = new ArrayList<String>();
        fileTable=new HashMap<String, Neighbour>();
        bsConnector = new BSConnector();
        udpSender = new UDPSender();
        decoder = new Decoder();
        lastSearch="";
    }

    /**
     * Receive JOIN message from another node
     * @param message
     */
    public void joinReceive(String message){
        String [] temp = message.split(" ");
        String neighbourIP = temp[2];
        System.out.println("Joined with:"+temp[3]);
        int neighbourPort = Integer.parseInt(temp[3]);
        Neighbour neighbour = new Neighbour(neighbourIP, neighbourPort, null);
        neighbours.add(neighbour);      // adding joined neighbour to the neighbour list
    }

    /**
     * Receive LEAVE message from another node
     * @param message
     */
    public void leaveReceive(String message){
        //message = "0033 LEAVE localhost 20043"
            String[] temp = message.split(" ");
            String neighbourIP = temp[2];
            int neighbourPort = Integer.parseInt(temp[3]);
            for (int i=0;i<neighbours.size();i++) {
                if (neighbours.get(i).getIp().equals(neighbourIP) && neighbours.get(i).getPort() == neighbourPort) {
                    neighbours.remove(neighbours.get(i));   // remove leaving node from peer list
                }
            }
            leaveSend(message);
            resetMsgCounts();
            totalTime = 0;
            hop = 0;
    }

    /**
     * Send Register message to Bootstrap server
     * @param serverIP
     * @param serverPort
     */
    public void registerBS(String serverIP, int serverPort){
        String tempMessage = "REG " +ip+ " "+ port + " " +username;
        int length = tempMessage.length()+5;
        String message = "00"+length+" " + tempMessage;
        String response, decoded;
        try {
            response = bsConnector.connectBS(message, serverIP, serverPort);
            decoded = decoder.decode(response);
            if(decoded.equals("REGOK")) { // if decoded message says registering was successful
                joinSend(response); // join with received nodes
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Send UnRegister message to Bootstrap server
     * @param serverIP
     * @param serverPort
     */
    public void unRegisterBS(String serverIP, int serverPort){
        //length UNREG IP_address port_no username
        String tempMessage = "UNREG " +ip+ " "+ port + " " +username;
        int length = tempMessage.length()+5;
        String message = "00"+length+" " +tempMessage;
        String response,decoded;
        try {
            response = bsConnector.connectBS(message, serverIP, serverPort);
            decoded = decoder.decode(response);
            if(decoded.equals("UNREG")){
                tempMessage = "LEAVE " +ip+ " "+ port+ " 3";
                length = tempMessage.length()+5;
                message = "00"+length+" "+ tempMessage;
                leaveSend(message); // sending leave message to peers

            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Send JOIN messages to the nodes receive from Bootstrap server
     * On return REGOK from bootstrap server
     * @param message
     */
    public void joinSend(String message){
        //message = "length REGOK no_nodes IP_1 port_1 IP_2 port_2"
        String [] temp = message.split(" ");
        String response = temp[1];
        int nNodes = Integer.parseInt(temp[2]);
        String IP_1 = null, IP_2 = null;
        int port_1 = 0, port_2 = 0;

        if(nNodes == 1){
            IP_1 = temp[3];
            port_1 = Integer.parseInt(temp[4]);
            //join message = length JOIN IP_address port_no
            String tempMessage1 = "JOIN " +ip+ " "+ port;
            int length = tempMessage1.length()+5;
            String message1 = "00"+length+" "+ tempMessage1;
            neighbours.add(new Neighbour(IP_1,port_1,null));
            udpSender.send(message1, IP_1, port_1);
        }else if(nNodes == 2){
            IP_1 = temp[3];
            port_1 = Integer.parseInt(temp[4]);
            IP_2 = temp[6];
            port_2 = Integer.parseInt(temp[7]);

            String tempMessage1 = "JOIN " +ip+ " "+port;
            int length1 = tempMessage1.length()+5;
            String message1 = "00"+length1+" "+ tempMessage1;
            neighbours.add(new Neighbour(IP_1,port_1,null));
            udpSender.send(message1, IP_1, port_1);

            String tempMessage2 = "JOIN " +ip+ " "+ port;
            int length2 = tempMessage2.length()+5;
            String message2 = "00"+length2+" "+ tempMessage2;
            neighbours.add(new Neighbour(IP_2,port_2,null));
            udpSender.send(message2, IP_2, port_2);
        }
    }

    /**
     * Send leave message to neighbors
     * @param message
     */
    public void leaveSend(String message){
        //message = length LEAVE IP_address port_no :TTL
        String data[] = message.split(" ");
        data[4]=Integer.toString(Integer.parseInt(data[4])-1);
        String tempMessage = data[1]+" "+data[2]+" "+data[3]+" "+data[4];
        int length = tempMessage.length()+5;
        message = "00"+length+" "+ tempMessage;
        if(Integer.parseInt(data[4])>0) { // checking hop count
            for (Neighbour neighbour : neighbours) {
                udpSender.send(message, neighbour.getIp(), neighbour.getPort());    // sending leave message to peers
            }
        }
    }

    /**
     * Receive message from another node
     * @param message
     */
    public void messageReceive(String message){
        messageCount++;
        String decoded= decoder.decode(message);
        if(decoded.equals("JOIN")){     // receive JOIN message from another node
            this.joinReceive(message);
        }else if(decoded.equals("LEAVE")){      // another node leaving
            this.leaveReceive(message);
        }else if(decoded.equals("SER")){        // receiving search request
            searchReceive(message);
        }else if(decoded.equals("SEROK")){      // receiving reply for search request sent
            searchOKReceive(message);
        }
    }

    /**
     * Search for Files
     * @param message
     */
    public void searchReceive(String message){

        List<String> result;
        String data[] = message.split(" ");
        String query="";
        String serId;
        String reply = " SEROK ";
        int hops;

        query=data[4];
        serId=data[data.length-2];
        if(!serId.equals(lastSearch)){
            lastSearch=serId;
            for(int i=5; i<data.length-2;i++) {
                query=query+" "+data[i];
            }
            System.out.println("Looking for:"+query);
            result=FileManager.getInstance().searchFile(query);
            if(result.isEmpty()) {
                reply = " SER "+data[2]+" "+data[3]+" "+query+" "+serId;
                //result=FileManager.getInstance().searchFile(query,files); //search in file table list
                //if(result.isEmpty()) {
                    hops=(Integer.parseInt(data[data.length-1])+1);
                    if(hops<=3){
                        System.out.println("File not found, sending request for neighbours");
                        reply = reply+" "+hops;
                        reply = setLength(reply)+reply;
                        for(Neighbour neighbour:neighbours) {
                            if(!neighbour.getIp().equals(data[2])||neighbour.getPort()!=Integer.parseInt(data[3])){ //avoid sending request to the sender
                                udpSender.send(reply,neighbour.getIp(),neighbour.getPort());
                                forwardMsg++;
                            }

                        }
                    }else{
                        //no result
                        reply += 0+" "+ip+" "+port;
                        reply = setLength(reply)+reply;
                        udpSender.send(reply,data[2],Integer.parseInt(data[3]));
                        ansMsg++;
                    }
//                }else{
//                    System.out.println("forwarding");
//                    //forward query
//                    for(String file: result) {
//                        Neighbour neighbour= fileTable.get(file);
//                        udpSender.send(reply,neighbour.getIp(),neighbour.getPort());
//                    }
//
//                }

            } else {
                reply += result.size()+" "+ip+" "+port;
                System.out.println("Found matching results");
                reply += (" "+data[data.length-1]);
                for(String file: result) {
                    reply += " "+file.replace(' ','_');
                }
                hops =Integer.parseInt(data[data.length-1]);
                reply += " "+hops;
                reply = setLength(reply)+reply;
                if(port==Integer.parseInt(data[3])&& ip.equals(data[2])) {
                    System.out.println("File found on Local files");
                } else {
                    udpSender.send(reply,data[2],Integer.parseInt(data[3]));
                    ansMsg ++;
                }
            }
        }

    }

    /**
     * Send request to search for file
     * @param query
     */
    public void searchSend(String query){
        sendTime = new Timestamp(System.currentTimeMillis());
        List<String> result;
        String request=" SER "+ip+" "+port+" "+query+" "+randomString(4)+" 1";
        request=setLength(request)+request;
        //search in file table
        //result=FileManager.getInstance().searchFile(query,files);
        //if(result.isEmpty()){
            for(Neighbour neighbour:neighbours) {
                udpSender.send(request,neighbour.getIp(),neighbour.getPort());
                forwardMsg++;
            }
//        } else{
//            for(String file: result) {
//                Neighbour neighbour= fileTable.get(file);
//                udpSender.send(request,neighbour.getIp(),neighbour.getPort());
//            }
//        }
    }

    /**
     * Adding files to local list when searchOK received
     * @param message
     */
    public void searchOKReceive(String message){
        //messgae = length SEROK no_files IP port hops filename1 filename2
        receiveTime = new Timestamp(System.currentTimeMillis());
        String [] temp = message.split(" ");
        String fileName = " ";
        System.out.println("  Match found on peer: " + temp[3] + " "+temp[4]);
        for(int i =6; i< temp.length-1; i++){
            fileName = temp[i].replace('_', ' ');
            System.out.println("    "+fileName);
            FileManager.getInstance().addFile(fileName); //  adding file to the local file list
            //files.add(fileName);
            //fileTable.put(fileName, new Neighbour(temp[3], Integer.parseInt(temp[4]), null));
        }
        searchOk ++;
        hop += Integer.parseInt(temp[temp.length-1]);
        totalTime = totalTime + (receiveTime.getTime() - sendTime.getTime());
    }
    /**
     * method to get message length in exactly 4 digits
     * @param message
     * @return
     */
    public String setLength(String message){
        String count = ""+(message.length()+4);
        while(count.length()<4){
            count ="0"+count;
        }
        return count;
    }

    public String getIp() {
        return ip;
    }

    public int getPort() {
        return port;
    }

    /**
     * Generate random string identifier
     * @param len
     * @return
     */
    private String randomString(int len )
    {
        String AB = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        Random rnd = new Random();
        StringBuilder sb = new StringBuilder( len );
        for( int i = 0; i < len; i++ )
            sb.append( AB.charAt( rnd.nextInt(AB.length()) ) );
        return sb.toString();
    }

    /**
     * Get files available on node
     * @return
     */
    public List<String> getFiles() {
        return files;
    }

    /**
     * Get peers of the node
     * @return
     */
    public ArrayList<Neighbour> getNeighbours() {
        return neighbours;
    }

    /**
     * Run  queries
     */
    public List<String> getQueries() {
        List<String> queries = new ArrayList<String>();
        try {

            InputStream stream = this.getClass().getResource("/"+"queries.txt").openStream();
            Scanner sc = new Scanner(stream, "UTF-8");
            String  s;
            while(sc.hasNextLine()) {
                s=sc.nextLine();
                queries.add(s);
            }
            sc.close();
        } catch (FileNotFoundException e) {
            System.out.println(e.getMessage());
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
        return queries;
    }

    public int getMessageCount() {
        System.out.println("Received:" + messageCount+"  Forwarded:"+forwardMsg+"  Answered:"+ansMsg);
        return messageCount;
    }

    public long getTotalTime() {
        return totalTime;
    }

    public int getHop() {
        return hop;
    }

    public void resetMsgCounts() {
        messageCount = 0;
        forwardMsg = 0;
        ansMsg = 0;
        hop = 0;
    }
}

