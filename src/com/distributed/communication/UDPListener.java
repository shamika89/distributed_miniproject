package com.distributed.communication;

import com.distributed.overlay.Node;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.Socket;

/**
 * User: Hp
 * Date: 3/6/15
 * Time: 7:42 AM
 */
public class UDPListener extends Thread{
    DatagramSocket socket;
    private Node node;

    public UDPListener(Node n) throws IOException {
        this.node=n;
        socket = new DatagramSocket(node.getPort());
        start();
    }

    @Override
    public void run() {
        while(true){
            try{
                byte[] buffer = new byte[1024];
                DatagramPacket packet = new DatagramPacket(buffer, buffer.length);
                socket.receive(packet);
                String message = new String(packet.getData(),0,packet.getLength());
                System.out.println(message);
                node.messageReceive(message);
            }catch(IOException e){
                System.out.println( e.getMessage());
            }
        }

    }

}
