package com.distributed.communication;

import java.io.IOException;
import java.net.*;

/**
 * User: Shamika
 * Date: 3/7/15
 * Time: 12:31 PM
 */
public class UDPSender {

    public void send(String message, String host, int port){
        try {
            InetAddress dst = InetAddress.getByName(host);
            byte[] buffer = message.getBytes();
            DatagramSocket socket = new DatagramSocket();
            DatagramPacket packet = new DatagramPacket(buffer, buffer.length, dst, port);
            socket.send(packet);
            socket.close();
        } catch (IOException e) {
            System.out.println("Error");
        }
    }
}
