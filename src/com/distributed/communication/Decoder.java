package com.distributed.communication;

import com.distributed.overlay.Neighbour;

import java.util.ArrayList;

/**
 * Created by Chathura on 3/7/2015.
 */
public class Decoder {

    /**
     * Message decoding
     * @param message
     */
    public String decode (String message) {

        String [] data = message.split(" ");
        ArrayList<Neighbour> neighbours;

        if(data.length >2) {

            //RESPONSE : REGISTER NODE
            if(data[1].equals("REGOK")) {

                int numberOfNodes = Integer.parseInt(data[2]);
                if(numberOfNodes ==0) {
                    System.out.println("Successfully registered");
                    System.out.println("First node to register");
                } else if(numberOfNodes == 1 || numberOfNodes == 2){
                    System.out.println("Successfully registered");
                    return "REGOK";
                } else if(numberOfNodes == 9999) {
                    System.out.println("Error in command");
                } else if(numberOfNodes == 9998) {
                    System.out.println("Already registered: Unregister first");
                }
            }

            //RESPONSE : UNREGISTER NODE
            else if (data[1].equals("UNROK")){
                int value = Integer.parseInt(data[2]);
                if(value == 0) {
                    System.out.println("Unregister successful");
                    return "UNREG";
                } else {
                    System.out.println("Error while unregistering");
                }
            }

            //RESPONSE : JOIN
            else if (data[1].equals("JOINOK")) {
                int value = Integer.parseInt(data[2]);
                if(value == 0) {
                    System.out.println("Join successful");
                } else {
                    System.out.println("Error while Joining");
                }
            }

            //RESPONSE : LEAVE
            else if (data[1].equals("LEAVEOK")) {
                int value = Integer.parseInt(data[2]);
                if(value == 0) {
                    System.out.println("Leave successful");
                } else {
                    System.out.println("Error while Leaving");
                }
            }

            //RESPONSE : SEARCH
            else if (data[1].equals("SEROK")) {
                int value = Integer.parseInt(data[2]);
                if(value == 0) {
                    System.out.println("No Matching Results");
                } else if (value == 9999){
                    System.out.println("Search failure : Node unreachable");
                } else if (value == 9998){
                    System.out.println("Unknown Error");
                } else {
                    System.out.println("File found");
                    return "SEROK";
                }
            }

            //REQUEST : SEARCH
            else if (data[1].equals("SER")){
                return "SER";
            }

            //RESPONSE : LEAVE NOTIFICATION BY PEER
            else if (data[1].equals("LEAVE")){
                return "LEAVE";
            }

            //REQUEST : JOIN
            else if (data[1].equals("JOIN")) {
                return "JOIN";
            }
        }
        else {
            System.out.println("Error in request");
        }
        return "";

    }
}
