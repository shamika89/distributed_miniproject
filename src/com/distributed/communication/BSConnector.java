package com.distributed.communication;

import java.io.*;
import java.net.Socket;

/**
 * User: Shamika
 * Date: 3/6/15
 * Time: 12:12 AM
 */
public class BSConnector {
    PrintWriter writer;
    BufferedReader br;
    Socket client;
    Decoder decoder;

    public String connectBS(String message, String serverName, int serverPort) throws IOException {
        //String message = "0032 REG localhost 20800 1234dbc";
        decoder = new Decoder();
        String response = "ERROR";
        char c = '\u0000';
        int i=0;
        try {
            System.out.println("Connecting to " + serverName + " on port " + serverPort);
            client= new Socket(serverName, serverPort);
            System.out.println("Just connected to " + client.getRemoteSocketAddress());
            writer = new PrintWriter(client.getOutputStream(), true);
            br = new BufferedReader(new InputStreamReader(client.getInputStream()));
            writer.println(message);

            char [] charBuf = new char[100];
            br.read(charBuf);
            response = "";
            while(charBuf[i]!=c){
                response+=charBuf[i];
                i++;
            }
            System.out.println(response);

        } catch (Exception e) {
            System.out.println("Error in connection" + e.getMessage());
        }
        finally {
            writer.close();
            br.close();
            client.close();
        }
        return response;
    }
}
