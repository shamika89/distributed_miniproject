package com.distributed.application;

import com.distributed.overlay.FileHandler;
import com.distributed.overlay.Neighbour;

import java.util.*;

/**
 * User: Randima
 * Date: 3/8/15
 * Time: 8:40 AM
 */
public class FileManager {
    private List<String> files;
    private List<String> searchedList;
    private static FileManager instance;

    private FileManager() {
        this.files=new ArrayList<String>();
        this.searchedList=new ArrayList<String>();
        this.initialize();
    }

    /**
     * Return instance of FileManger
     * @return
     */
    public static FileManager getInstance(){
        if(instance==null){
            instance=new FileManager();
        }
        return instance;
    }

    /**
     * Search files that match with filename
     * @param filename
     * @return
     */
    public List<String> searchFile(String filename){
        Set<String> filesSet;
        List<String> result = new ArrayList<String>();
        Set<String> query = new HashSet<String>(Arrays.asList(filename.toLowerCase().split("[\\.\\s\\_-]")));
        for(String file:files) {
            filesSet = new HashSet<String>(Arrays.asList(file.toLowerCase().split("[\\.\\s\\_-]")));
            if(filesSet.containsAll(query)) {
                result.add(file);
            }
        }
        return result;
    }

    public List<String> searchFile(String filename,List<String> fileList){
        Set<String> filesSet;
        List<String> result = new ArrayList<String>();
        Set<String> query = new HashSet<String>(Arrays.asList(filename.toLowerCase().split("[\\.\\s\\_-]")));
        for(String file:fileList) {
            filesSet = new HashSet<String>(Arrays.asList(file.toLowerCase().split("[\\.\\s\\_-]")));
            if(filesSet.containsAll(query)) {
                result.add(file);
            }
        }
        return result;
    }

    /**
     * initialize class
     */
    private void initialize(){
        FileHandler fh=FileHandler.getInstance();
        files=fh.getRandomFileList(5);
    }

    /**
     * gives current file list
     * @return
     */
    public List<String> getFiles() {
        return files;
    }

    public void addFile(String name){
        if(!searchedList.contains(name)) {
            files.add(name);
            searchedList.add(name);
        }
    }

    /**
     * select five random files from all file names
     * @return
     */
    public List<String> randomize(){
        FileHandler fh=FileHandler.getInstance();
        files=fh.getRandomFileList(5);
//        files.addAll(searchedList);
        for(String s:searchedList) {
            if(!files.contains(s)){
                files.add(s);
            }
        }
        return files;
    }



}
