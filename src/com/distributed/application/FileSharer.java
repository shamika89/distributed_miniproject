package com.distributed.application;

import com.distributed.communication.Decoder;
import com.distributed.communication.UDPListener;
import com.distributed.overlay.Neighbour;
import com.distributed.overlay.Node;
import com.sun.org.apache.xpath.internal.SourceTree;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.*;

/**
 * User: Shamika
 * Date: 3/7/15
 * Time: 3:51 PM
 */
public class FileSharer {

    public static void main(String[] args) throws IOException {
        FileManager fm=FileManager.getInstance();

        String nodeIp = args[0];
        int nodePort = Integer.parseInt(args[1]);
        String nodeUsername = args[2];

        String serverIP = args[3];
        int serverPort = Integer.parseInt(args[4]);

        Node node = new Node(nodeIp, nodePort, nodeUsername);
        UDPListener listener = new UDPListener(node);

        node.registerBS(serverIP, serverPort); // registering the node on BS server
        fm.randomize(); // initializing file list.


        Scanner scanner = new Scanner(System.in);
        String cmd,value;
        String[] tokens;
        while(true){
            cmd = scanner.nextLine();
            tokens=cmd.split(" ");
            if(tokens[0].toLowerCase().equals("ser") && tokens.length !=1){
                value=cmd.substring(tokens[0].length()+1);
                System.out.println("Searching:"+value);
                List<String> rslt=fm.searchFile(value);
                if(rslt.isEmpty()){
                    // System.out.println("no files found");
                    node.searchSend(value);
                }else{
                    System.out.println("File found on local files");
                }
            }else if(tokens[0].toLowerCase().equals("leave")){
                node.unRegisterBS(serverIP,serverPort);
                System.out.println("disconnected");
                System.exit(0);
            }else if(tokens[0].toLowerCase().equals("rand")){
                System.out.println("file list randomized");
                fm.randomize();
            }else if(tokens[0].toLowerCase().equals("ls")){
                System.out.println("File list:");
                for(String s:FileManager.getInstance().getFiles()){
                    System.out.println(s);
                }
            }else if(tokens[0].toLowerCase().equals("query")){
                System.out.println("Running queries");
                List<String> queries;
                queries = node.getQueries();
                //node.resetMsgCounts();
                int hops;
                for (String query : queries) {
                    System.out.println("    Searching for: "+query);
                    List<String> rslt = fm.searchFile(query);
                    if (rslt.isEmpty()) {
                        // System.out.println("no files found");
                        node.searchSend(query);
                        try {
                            Thread.sleep(1000);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    } else {
                        System.out.println("File found on local files");
                    }
                }
            }else if(tokens[0].toLowerCase().equals("peers")){
                System.out.println("Peers List:");
                for(Neighbour s:node.getNeighbours()){
                    System.out.println(s.getIp()+"  "+s.getPort());
                }
            }else if(tokens[0].toLowerCase().equals("msg")){
                System.out.println(node.getMessageCount());
            }else if(tokens[0].toLowerCase().equals("time")){
                System.out.println(node.getTotalTime());
            }else if(tokens[0].toLowerCase().equals("npeers")){
                System.out.println(node.getNeighbours().size());
            }else if(tokens[0].toLowerCase().equals("hops")){
                System.out.println(node.getHop());
            }else if(tokens[0].toLowerCase().equals("help")){
                System.out.println("Following commands are not case sensitive");
                String format = "%-13s%s%n";
                System.out.printf(format,"LEAVE","Node will leave the network");
                System.out.printf(format,"LS","Show the file list that the node has");
                System.out.printf(format,"RAND","Randomize the  file list that the node has");
                System.out.printf(format,"SER <query>","Search for <query> on the network");
                System.out.printf(format,"PEERS","Display currently connected nodes");
                System.out.printf(format,"QUERY","runs query set on node");
                System.out.printf(format,"MSG","Display current Message counts");

            }
            else{
                System.out.println("Command not found!. Try HELP for commands list");
            }
        }
    }
}

